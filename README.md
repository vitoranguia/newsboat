# Newsboat 

My simple configuration [Newsboat](https://newsboat.org) with NextCloud News

![Print screen](https://pixelfed.social/storage/m/113a3e2124a33b1f5511e531953f5ee48456e0c7/85c26953a179a0e533565ce79e82deb9724813d5/7UouQFy2W0U4HfrdEIyKpEm8ulb1MwshPphJWyM7.png)

Requirements:

* [NextCloud](https://nextcloud.com)
* [NextCloud News](https://apps.nextcloud.com/apps/news)

Install

```
# apt install -y newsboat
```

```
$ cp -r .newsboat "$HOME"/
```

Change the uppercase words in the `"$HOME"/.newsboat/config` file,
according to your NextCloud provider
```
ocnews-url "PROVIDER"
ocnews-login "USER"
ocnews-password "PASSWORD"
```

Refresh on star

```
$ newsboat -r
```


Shortcuts

<kbd>?</kbd> help

<kbd>ENTER</kbd> open article

<kbd>q</kbd> quit

<kbd>R</kbd> reload all sources

<kbd>n</kbd> open next not reader

<kbd>N</kbd> toggle article read/not read

<kbd>s</kbd> save article

<kbd>u</kbd> show all URLs

<kbd>o</kbd> open article in web browser

<kbd>/</kbd> search
